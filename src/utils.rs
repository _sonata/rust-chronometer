pub fn total_seconds(hours: u32,
    minutes: u32,
    seconds: u32) -> u32 {
       hours * 3600 + minutes * 60 + seconds
    }