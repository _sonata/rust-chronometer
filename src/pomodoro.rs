use std::{thread, time};
use std::process;
use std::io::{stdout};

use crossterm::{
    ExecutableCommand,
    terminal,
    cursor,
    style::Print,
};

use crate::utils;

pub struct Pomodoro {
    hours: u32,
    minutes: u32,
    seconds: u32,
}

impl Pomodoro{
    pub fn prompt_values() -> (u32, u32) {
        (25, 5)
    }

    pub fn launch(work: u32, pause: u32) -> std::io::Result<()> {
        Self::render(work)
    }

    fn to_tuple(&self) -> (u32, u32, u32) {
        (self.hours, self.minutes, self.seconds)
    }

    pub fn render(mut minutes: u32) -> std::io::Result<()> {
        let mut seconds = 0;
        let mut hours = minutes / 60;
        minutes = minutes % 60;
        let mut total_seconds = utils::total_seconds(hours, minutes, seconds);

        // prep text
        let mut buf = format!("{hours}h {minutes}m {seconds}s");

        //place cursor
        let mut stdout = stdout();

        _ = stdout.execute(Print(buf))?
            .execute(cursor::MoveLeft(100))?
            .execute(cursor::Hide);

        // duration of 1s
        let pause: time::Duration = time::Duration::from_secs(1);
        let mut total_seconds = utils::total_seconds(hours, minutes, seconds);
        while total_seconds > 0 {

            thread::sleep(pause);
            total_seconds -= 1;

            // Calculate new time values
            match seconds {
                0 => {
                    seconds = 59;
                    minutes -= 1;
                    if minutes == 0 && hours > 0{
                        minutes = 59;
                        hours -= 1;
                    }
                },
                _ => seconds -= 1,
            }

            buf = format!("{hours}h {minutes}m {seconds}s");
            _ = stdout.execute(terminal::Clear(terminal::ClearType::CurrentLine))?
                .execute(Print(buf))?
                .execute(cursor::MoveLeft(100));
        }

        stdout.execute(cursor::Show)?;

        println!("\nChronometer has ended !\n");
        Ok(())
    }
}