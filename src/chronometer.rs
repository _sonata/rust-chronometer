use std::{thread, time};
use std::process;
use std::io::stdout;

use clap::ArgMatches;
use crossterm::{
    ExecutableCommand,
    terminal,
    cursor,
    style::Print,
};

use crate::utils;
use crate::Args;

pub struct Chronometer {
    hours: u32,
    minutes: u32,
    seconds: u32,
}

impl Chronometer {
    pub fn parse_time_args(seconds: u32,
                           minutes: u32, 
                           hours: u32,
                        ) -> Self {    
        // Sanitize times
        let mut minutes = minutes + seconds / 60;
        let seconds = seconds % 60;
        let hours = hours + minutes / 60;
        minutes += minutes % 60;
    
        Chronometer {
            hours: hours,
            minutes: minutes,
            seconds: seconds,
        }
    }
    
    fn to_tuple(&self) -> (u32, u32, u32) {
       (self.hours, self.minutes, self.seconds)
    }

    pub fn render(&mut self) -> std::io::Result<()> {
        let (mut hours, mut minutes, mut seconds) = self.to_tuple();

        // prep text
        let mut buf = format!("{hours}h {minutes}m {seconds}s");
    
        //place cursor
        let mut stdout = stdout();
    
        _ = stdout.execute(Print(buf))?
            .execute(cursor::MoveLeft(100))?
            .execute(cursor::Hide);
    
        // duration of 1s
        let pause: time::Duration = time::Duration::from_secs(1);
        let mut total_seconds = utils::total_seconds(hours, minutes, seconds);
        while total_seconds > 0 {
    
            thread::sleep(pause);
            total_seconds -= 1;
    
            // Calculate new time values
            match seconds {
                0 => {
                    seconds = 59;
                    minutes -= 1;
                    if minutes == 0 && hours > 0 {
                        minutes = 59;
                        hours -= 1;
                    }
                },
                _ => seconds -= 1,
            }
    
    
            buf = format!("{hours}h {minutes}m {seconds}s");
            _ = stdout.execute(terminal::Clear(terminal::ClearType::CurrentLine))?
                .execute(Print(buf))?
                .execute(cursor::MoveLeft(100));
        }
    
        stdout.execute(cursor::Show)?;
        
        println!("\nChronometer has ended !\n");
        Ok(())
    }

}
