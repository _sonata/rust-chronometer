use std::env;
use clap::{arg, command, value_parser, ArgAction, Command, ArgMatches};
use log::{info, warn, debug};

mod chronometer;
mod pomodoro;
mod utils;

pub use crate::chronometer::Chronometer;
pub use crate::pomodoro::Pomodoro;

struct Args {
    seconds: u32,
    minutes: u32,
    hours: u32,
    pomodoro: bool,
}

fn argparsing() -> Args{
    let matches = command!() 
        .arg(
            arg!(
                -p --pomodoro "If set, launch a Pomodoro timer"
            )
            // We don't have syntax yet for optional options, so manually calling `required`
            .required(false)
            .value_parser(value_parser!(bool)),
        )
        .arg(arg!(
            -s --seconds ... "Seconds for the timer"
            )
            .required(false)
            .value_parser(value_parser!(u32)),
        )
        .arg(arg!(
                -m --minutes ... "Minutes for the timer"
            )
            .required(false)
            .value_parser(value_parser!(u32)),
        )
        .arg(arg!(
            -h --hours ... "Hours for the timer"
        )
        .required(false)
        .value_parser(value_parser!(u32)),
    )
    .get_matches();

    debug!("{:?}", matches);
    // Create the Args struct
    
    Args {
        seconds: 0,
        minutes: 0,
        hours: 0,
        pomodoro: false,
    }

}

fn main() -> std::io::Result<()> {
    // get program arguments
    let args = argparsing();

    // Option : Enter Pomodoro if option -p exists
    if args.pomodoro {
        // Enter Pomodoro option
        let (work,
             pause) = Pomodoro::prompt_values();

        Pomodoro::launch(work, pause)

    } else {
        // manage arguments to transform into values
        let mut chrono = Chronometer::parse_time_args(args.seconds,
                                                                    args.minutes,
                                                                    args.hours
                                                                    );
        println!("Launching Chronometer...");
        chrono.render()
    }
}